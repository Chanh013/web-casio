export const imageDefault =
  "https://www.hay8866.com/server/static/img/thirdgame/luckywin/dxLive.png?v=202401012";
export const listInfoFeatureds = [
  {
    title: "Game nổi bật",
    key: "game",
    listGames: [
      "https://www.hay8866.com/server/static/img/thirdgame/luckywin/dxLive.png?v=202401012",
      "https://www.hay8866.com/server/static/img/thirdgame/luckywin/sdLive.png?v=20240105",
      "https://www.hay8866.com/server/static/img/thirdgame/luckywin/MINIPOKER.png?v=202401012",
      "https://www.hay8866.com/server/static/img/thirdgame/luckywin/BACCARAT.png?v=202401012",
      "https://www.hay8866.com/server/static/img/thirdgame/luckywin/TAIXIU.png?v=202401012",
      "https://www.hay8866.com/server/static/img/thirdgame/luckywin/XOCDIA.png?v=202401012",
      "https://www.hay8866.com/server/static/img/thirdgame/luckywin/SICBO.png?v=202401012",
      "https://www.hay8866.com/server/static/img/thirdgame/luckywin/BAUCUA.png?v=202401012",
      "https://www.hay8866.com/server/static/img/thirdgame/luckywin/BAUCUA.png?v=202401012",
      "https://www.hay8866.com/server/static/img/thirdgame/luckywin/BAUCUA.png?v=202401012",
      "https://www.hay8866.com/server/static/img/thirdgame/luckywin/BAUCUA.png?v=202401012",
      "https://www.hay8866.com/server/static/img/thirdgame/luckywin/BAUCUA.png?v=202401012",
      "https://www.hay8866.com/server/static/img/thirdgame/luckywin/BAUCUA.png?v=202401012",
      "https://www.hay8866.com/server/static/img/thirdgame/luckywin/BAUCUA.png?v=202401012",
    ],
  },

  {
    title: "Sắp mở thưởng",
    key: "lucky",
    listLuckies: [
      {
        timeSibo: "Sicbo 20 giây",
        image:
          "https://www.hay8866.com/server/static/img/thirdgame/luckywin/dxLive.png?v=202401012",
        types: ["Kèo đôi", "Tổng", "Số Đơn"],
        listDices: [7, "Xỉu", "Lẻ", "XX1", "XX2", "XX3"],
      },
      {
        timeSibo: "Sicbo 20 giây",
        image:
          "https://www.hay8866.com/server/static/img/thirdgame/luckywin/dxLive.png?v=202401012",
        types: ["Kèo đôi", "Tổng", "Số Đơn"],
        listDices: [7, "Xỉu", "Lẻ", "XX1", "XX2", "XX3"],
      },
      {
        timeSibo: "Sicbo 20 giây",
        image:
          "https://www.hay8866.com/server/static/img/thirdgame/luckywin/dxLive.png?v=202401012",
        types: ["Kèo đôi", "Tổng", "Số Đơn"],
        listDices: [7, "Xỉu", "Lẻ", "XX1", "XX2", "XX3"],
      },
      {
        timeSibo: "Sicbo 20 giây",
        image:
          "https://www.hay8866.com/server/static/img/thirdgame/luckywin/dxLive.png?v=202401012",
        types: ["Kèo đôi", "Tổng", "Số Đơn"],
        listDices: [7, "Xỉu", "Lẻ", "XX1", "XX2", "XX3"],
      },
      {
        timeSibo: "Sicbo 20 giây",
        image:
          "https://www.hay8866.com/server/static/img/thirdgame/luckywin/dxLive.png?v=202401012",
        types: ["Kèo đôi", "Tổng", "Số Đơn"],
        listDices: [7, "Xỉu", "Lẻ", "XX1", "XX2", "XX3"],
      },
      {
        timeSibo: "Sicbo 20 giây",
        image:
          "https://www.hay8866.com/server/static/img/thirdgame/luckywin/dxLive.png?v=202401012",
        types: ["Kèo đôi", "Tổng", "Số Đơn"],
        listDices: [7, "Xỉu", "Lẻ", "XX1", "XX2", "XX3"],
      },
    ],
  },
  {
    title: "Casino trực tuyến",
    key: "casino",
    listCasinoes: [
      {
        imageLogo:
          "https://www.hay8866.com/server/static/img/thirdgame/imgs/platform_logo/long/wm.png?v=202401012",
        name: "WM CASINO",
        vat: "Phí hoàn tối đa 1.1%",
      },
      {
        imageLogo:
          "https://www.hay8866.com/server/static/img/thirdgame/imgs/platform_logo/long/wm.png?v=202401012",
        name: "WM CASINO",
        vat: "Phí hoàn tối đa 1.1%",
      },
      {
        imageLogo:
          "https://www.hay8866.com/server/static/img/thirdgame/imgs/platform_logo/long/wm.png?v=202401012",
        name: "WM CASINO",
        vat: "Phí hoàn tối đa 1.1%",
      },
      {
        imageLogo:
          "https://www.hay8866.com/server/static/img/thirdgame/imgs/platform_logo/long/wm.png?v=202401012",
        name: "WM CASINO",
        vat: "Phí hoàn tối đa 1.1%",
      },
    ],
  },
  {
    title: "Thể thao",
    key: "sport",
    listSports: [
      {
        name: "FB Sports",
        imageLogo:
          "https://www.hay8866.com/server/static/img/thirdgame/imgs/platform_logo/long/fbsports.png?v=202401012",
        vat: "Phí hoàn tối đa 1.1%",
        image:
          "https://www.hay8866.com/server/static/img/thirdgame/imgs/pc/sports/athlete/fbsports.png?v=202401012",
      },
      {
        name: "SABA Sports",
        imageLogo:
          "https://www.hay8866.com/server/static/img/thirdgame/imgs/platform_logo/long/wm.png?v=202401012",
        vat: "Phí hoàn tối đa 1.1%",
        image:
          "https://www.hay8866.com/server/static/img/thirdgame/imgs/pc/sports/athlete/fbsports.png?v=202401012",
      },
      {
        name: "CMD Thể thao",
        imageLogo:
          "https://www.hay8866.com/server/static/img/thirdgame/imgs/platform_logo/long/wm.png?v=202401012",
        vat: "Phí hoàn tối đa 1.1%",
        image:
          "https://www.hay8866.com/server/static/img/thirdgame/imgs/pc/sports/athlete/fbsports.png?v=202401012",
      },
    ],
  },
];

export const listMenu = [
  {
    url: "#home",
    name: "Trang chủ",
  },
  {
    url: "#xo-so",
    name: "Xổ số",
    children: [
      {
        url: "#service1",
        name: "Service1",
      },
      {
        url: "#service2",
        name: "Service2",
      },
      {
        url: "#service3",
        name: "Service3",
      },
    ],
  },
  {
    url: "#sport",
    name: "Thể thao",
    children: [
      {
        url: "#service1",
        name: "Service1",
      },
      {
        url: "#service2",
        name: "Service2",
      },
      {
        url: "#service3",
        name: "Service3",
      },
    ],
  },
  {
    url: "#luckywin",
    name: "Luckywin",
    children: [
      {
        url: "#service1",
        name: "Service1",
      },
      {
        url: "#service2",
        name: "Service2",
      },
      {
        url: "#service3",
        name: "Service3",
      },
    ],
  },
  {
    url: "#casino",
    name: "Casino",
    children: [
      {
        url: "#service1",
        name: "Service1",
      },
      {
        url: "#service2",
        name: "Service2",
      },
      {
        url: "#service3",
        name: "Service3",
      },
    ],
  },
  {
    url: "#no-hu",
    name: "Nổ hũ",
    children: [
      {
        url: "#service1",
        name: "Service1",
      },
      {
        url: "#service2",
        name: "Service2",
      },
      {
        url: "#service3",
        name: "Service3",
      },
    ],
  },
  {
    url: "#game",
    name: "Game bài",
    children: [
      {
        url: "#service1",
        name: "Service1",
      },
      {
        url: "#service2",
        name: "Service2",
      },
      {
        url: "#service3",
        name: "Service3",
      },
    ],
  },
  {
    url: "#ban-ca",
    name: "Bắn cá",
    children: [
      {
        url: "#service1",
        name: "Service1",
      },
      {
        url: "#service2",
        name: "Service2",
      },
      {
        url: "#service3",
        name: "Service3",
      },
    ],
  },
];

export const listLogoFooter = [
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
  23, 24, 25,
];

export const listQrCode = [
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUAAAAFXCAIAAABcIQDeAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAQUklEQVR4nO3dcUhd5R/H8aPT26YW0cYG5twaZU6WWBs1RJbJGCHLhkGERMQwKPCPiOiPiCKQimB/JBEStYJqDP8YJjIobMSykHE3LIQ5iiabjWVjW7WGOOv0x4H7u797vc/t+pznnvO5vl9/Xc/xnPOc597P7pl8+T5lvu97ADSVRz0AAMtHgAFhBBgQRoABYQQYEEaAAWEEGBBGgAFhBBgQRoABYQQYEEaAAWEEGBBGgAFlfiGiHmz4kslkQTNQnLlyNyp3Y47qfqMaszsF3T7fwIAwAgwII8CAMAIMCCPAgDACDAgjwIAwAgwII8CAsIoQz/XJJ59s3bo1xBOGYseOHVEPYQnmubIZczKZ3L59e669ZWVljkZl8+6bz2y+I7OS/0yGGeCtW7cue6JXmnjOlc2oSu+OJPAIDQgjwIAwAgwII8CAMAIMCCPAgDACDAgjwICwMAs5zMw1QDZsKnXMzGM2XzeZTBqOtanFcVfFZWZzR+7eIxuKn8kMxQvwSuPu/YuquiiGCQSP0IAwAgwII8CAMAIMCCPAgDACDAgjwIAwAgwIo5DDFXdVPori2ZmsBPANDAgjwIAwAgwII8CAMAIMCCPAgDACDAgjwIAwAgwIoxLLxNwFqrGxsWgjSeeun5b5jmxWNjSjTmvZCLBJ6XWBctdPq+TXAYwnHqEBYQQYEEaAAWEEGBBGgAFhBBgQRoABYQQYEFa8Qg5zVZONqCqibES1wqCZTR+vqOrDbJTAZ7J4AaZMJx11S+lYb3HZeIQGhBFgQBgBBoQRYEAYAQaEEWBAGAEGhBFgQFiYhRyl19nIXJmUTCYjqQSIqorL5ro2x9rMc+l9JjPQE0tPVHVLNtel8swRHqEBYQQYEEaAAWEEGBBGgAFhBBgQRoABYQQYEFZYIYe7HkJRMfcuMt+vu9ojmx5R7sZsc92oji15Zb7vRz0GVTaFllEVacazOBTLxiM0IIwAA8IIMCCMAAPCCDAgjAADwggwIIwAA8Li0lLHZl08m5IJmzPb1A/ZsLkjd+sA2ozKLKryEnefnBDFJcCKFIuW6E1VYniEBoQRYEAYAQaEEWBAGAEGhBFgQBgBBoQRYEBYYYUc8WzI4q43lZm72XC3pp7NmW3q0syiehfMx0a1CmRBSr8Si9qj4ii9SZb45PAIDQgjwIAwAgwII8CAMAIMCCPAgDACDAgjwICwwhY3c9f3yN1afpbr8VVXV+fae/LkScOZo6rUMd+RDXdVXOaZtHkX3L2DNp+6MGsW/UKEc8mlJJPJZV/X3bE2wpubwsTzjtyNyh2bOyrabPAIDQgjwIAwAgwII8CAMAIMCCPAgDACDAgjwICwwlrqmPseRVUvFVVPrHj2iIpKVOse2lQ1RdUTy3xs3jqQdIUF2Kb+y6bDUFTH2oh/O6VwSXSQKojEHfEIDQgjwIAwAgwII8CAMAIMCCPAgDACDAgjwICwwnpimdl0J7LptqVYp2VmMxs26/GZRTVX7jqxxZPDSiyzqMpWFOu0FDFXMcQjNCCMAAPCCDAgjAADwggwIIwAA8IIMCCMAAPCCivkcFchZMNy/UHDsTZ9rWy6QEV1XXe1Vu46ZtlcN6pObGEqaCk0mwvZrObm7rorTVTvkc274O66UR0bIh6hAWEEGBBGgAFhBBgQRoABYQQYEEaAAWEEGBBWvNUJbdjUtSiu9GfmroNU6c1VVB3RzMeGWJVYvNUJbdCNCS6UwOeKR2hAGAEGhBFgQBgBBoQRYEAYAQaEEWBAGAEGhIW5OmFUK9+Z61rMayYqslnn0V3tkTtR1eG5u25klVjuuKuJUS+1CZdi7VFUY5aYKx6hAWEEGBBGgAFhBBgQRoABYQQYEEaAAWEEGBAW5uqE7ipXoqofsuk+5e5Yd2x6nkVVL2VT/2cjqutmCLMSy13likRNTAmwmWTeo0jwCA0II8CAMAIMCCPAgDACDAgjwIAwAgwII8CAsDALOYrWB6ggNlVc8Vzbzsw8qsbGRsOxMakuyhDbKr1lH2seVUFt6uLSE8udlVYfFs9RuRPV/cZkknmEBoQRYEAYAQaEEWBAGAEGhBFgQBgBBoQRYEBYYasTxrNSx4ZNJyd3qwTacLeSo7tKO3efq3iuMGi+XyqxCsDKd+liOCRL8ZznEPEIDQgjwIAwAgwII8CAMAIMCCPAgDACDAgjwICwuFRi2XQ2impdPHdsVjbM2xPLUfWY+cxmNhVg7ri73xArsTy/EMsb7n+RTCaXfV3zsVHdkY14zobNmW2EN6+FcXe/5usWdCoeoQFhBBgQRoABYQQYEEaAAWEEGBBGgAFhBBgQVryWOjar5plXgnNXqeOuc5VZVLVH5nm2WdnQprYsKhKfq+IF2KY7UVRtjUq+o1KGFXWzEQrxc8UjNCCMAAPCCDAgjAADwggwIIwAA8IIMCCMAAPCCuuJZe5dZGbTQ8jMZlRmNrU47vpamSmuIWgW1UqOUSkokoVVYsWzUieeo7Kx0irAzJgNAx6hAWEEGBBGgAFhBBgQRoABYQQYEEaAAWEEGBBWWCFHPHsX2bCpTHLHXYWQzZltVpB0x6YXl42YzEbxemJBHRVR6WIyGzxCA8IIMCCMAAPCCDAgjAADwggwIIwAA8IIMCAszEKOqNbyM7Psa7XsY23WW3Qnnl2gbNisqBiVmK5OGJPalBC5u50Sm6gIKc4kqxMC8DwCDEgjwIAwAgwII8CAMAIMCCPAgDACDAgrXksdd/203PU9sum35O5YG+7WPXR35nhWj9nMhvlYh6sTQp27arnSq8OzUbTZ4BEaEEaAAWEEGBBGgAFhBBgQRoABYQQYEEaAAWEUciyfTS2OTa8md+vimSvAbO7I5sxm7iqibI51V7WWgQC7Uno1T4p3ZHPmeI4qA4/QgDACDAgjwIAwAgwII8CAMAIMCCPAgDACDAijkMPEvPKdu15NUa24F8+V/myquGzeI3fvLz2xiiSqJk8r7bo2VngvLh6hAWEEGBBGgAFhBBgQRoABYQQYEEaAAWEEGBBWvEIOc5WPjagqhGzYrD8Y1WqMNqLqTWUzKrOorpuheAFeyeUyKPneVFFdl0doQBgBBoQRYEAYAQaEEWBAGAEGhBFgQBgBBoSFWcjhrocQ0kXVi8ssqg5SUVVTmblbmzIDPbHwP4rVcvHsiVW0UfEIDQgjwIAwAgwII8CAMAIMCCPAgDACDAgjwICwsoKWQgMQK3wDA8IIMCCMAAPCCDAgjAADwggwIIwAA8IIMCCMAAPCCDAgjAADwggwIIwAA8IIMCCMAGc6e/bsPffcE/UoPM/zvv3227vvvjvXlviMs2iyJwQEONPCwsLU1NSSu65cufLOO++Yt4RoeHh43759ubYYxrk8N27ceO211zZt2lRZWXnHHXe8/vrrN27cyLurmLInBJ6P/zc9PZ1rWrJ3GX7ZXkNDw/j4eK4toV+6r69v27Ztx48fv3DhwujoaH19/QsvvJB3VzFlTwgIcKaYBHh6enrdunWLi4u5toR+6VtuueXrr79O/Tg6Onrrrbfm3VU02RMC3/d5hF7aL7/88thjj61Zs2bjxo1fffWV53kHDhwI1qQqKysrKyv77LPPsrd4nnfmzJm77rrr+++/f+SRR9asWbNhw4YDBw6kTnv+/PlHH3305ptvrq6ufuihhz7//PNg+19//XXlypX0AQwPD+/du3fVqlWGLUuO0/O806dPP/PMMxs3bqysrHzggQfOnDkTbL/33nvffPPN9MOfe+651H+ky8vLr1+/ntq1fv36f/75J++udLmum3dazHsNtw++gTMF32wNDQ39/f3Dw8O7d+/esmWL7/tzc3NHjhzxPG9ycnJycvLy5cvZW4LDKyoqamtr33777bGxsf7+/oqKikOHDgUn37lz5+OPP37ixIkTJ068+uqrL774YrB9y5Ytt91227Vr11LDaG1tHR4eTh9YxpZc4/R9v6Ojo6en58iRI2NjY3v27GltbQ22DwwMNDQ0pJ+ztrb2rbfeCl4///zzdXV1x44dC37s6el59tln8+5Kl+u6eafFvDfXhMDnETpbEIxkMhn8+NNPP3med/HiRf+/PUIHW2ZnZ1NbXn755V27dgWvE4nEd999l33Rtra2xsbG+fn54MeLFy9WVVWl5zl7i2GcCwsLqV+bmZnxPO/q1au+71+6dCmRSKT+GxksJjozMxP8OD8/Hyy32dLSsnfv3h07dvzxxx95d6XLdd2802Leu+TtI8Aj9NJSa0Nu3rzZ87yrV68WdPjtt9+eet3Z2Tk5ORm8fuKJJ3p6ej788MPff/89/fe/+eab06dP33TTTcGPo6OjHR0d1dXVqV/I3mIYZ2Vlped5Z8+e/eijjwYGBjzPm5ub8zxv7dq13d3dH3/8cXDIyMhIW1vbpk2bUj/Ozc0dPXq0o6NjfHx8ZmYm+EfBvCtdruvmnZa8e3PdPvgGzpT9pep53vT09JK7/suWH374oby8PHi9uLg4ODjY3Ny8evXq3t7eS5cuLTmGrq6u999/37zFMM7Z2dmHH3548+bNfX19H3zwQWq77/tffvllTU1N8FXW0tIyODiYOryurm5kZCR4ffny5c7Ozqampry70hmua54W894lbx8BApwp9ACPjo7W1tZmXCWZTO7cubOjoyN7ANeuXauqqgoehnNtMY+zvb29r68v9Qfb9CAtLi7W19d/+umns7OziUQi9S9I8MSbeob3ff/ChQue583Ozhp2ZYzccF3ztJj3Lnn7CPAIXYDy8nLP89JrGLK3BP7+++/U64MHD7a3t2ds3759+8DAwPj4ePDjr7/+ev78+eD12NhYc3Pzhg0bUmfI3mI2Pj7e29sb/MH2zz//TN+1atWqp59++tChQ0ePHt2zZ8/atWuD7TU1NZ7n/fzzz6nfXFhY8DwvkUgYdmWM3HBd87SY9xZ6+ytL1P+CxI7hm21+fr6qqqq/v//UqVPB35yztwSHt7W1DQ0NHTt2bP/+/atXr56amgpOdf/99w8ODk5NTU1MTHR1dbW3twfb6+rqUk+2+/fvf+ONN9IHkL3FPM6mpqannnoqmUwePnx427ZtiUQi9U3o+/7MzEwikWhtbT18+HD64T09PU1NTWNjY+fOnRsbG7vvvvu6u7vz7kofueG65mkx713y9hEgwJkMwfB9/+DBg+vWrauqqnrvvfeW3BIcPjQ01NzcXF5e3tLScvz48dSpTp061dXVVVNTU1VV1dXVlXoKTf0VenFxcf369anPru/72VvyjnNiYqKxsbGiomLXrl0TExNNTU3pAfZ9f/fu3anUpczPz7/yyiv19fUVFRV1dXUvvfTS9evX8+5K//u54brmaTHszXX7CBDgkFkWSI2Pj995553mLfZ6e3uffPLJcM9pZp4Ww14Xt19KKor+zA6T4eHhrq4u8xZLv/3229DQ0MjISIjndCf02y8x/BErXkZGRjI+r9lblu3HH3/84osvOjs729vbH3zwwVDO6VqIt1+S+AaOl/T64Vxblm3fvn3nzp3r7u5+9913wzqnayHefklifWBAGI/QgDACDAgjwIAwAgwII8CAsH8BaQkuMIIwmRwAAAAASUVORK5CYII=",
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUAAAAFXCAIAAABcIQDeAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAQUklEQVR4nO3dcUhd5R/H8aPT26YW0cYG5twaZU6WWBs1RJbJGCHLhkGERMQwKPCPiOiPiCKQimB/JBEStYJqDP8YJjIobMSykHE3LIQ5iiabjWVjW7WGOOv0x4H7u797vc/t+pznnvO5vl9/Xc/xnPOc597P7pl8+T5lvu97ADSVRz0AAMtHgAFhBBgQRoABYQQYEEaAAWEEGBBGgAFhBBgQRoABYQQYEEaAAWEEGBBGgAFlfiGiHmz4kslkQTNQnLlyNyp3Y47qfqMaszsF3T7fwIAwAgwII8CAMAIMCCPAgDACDAgjwIAwAgwII8CAsIoQz/XJJ59s3bo1xBOGYseOHVEPYQnmubIZczKZ3L59e669ZWVljkZl8+6bz2y+I7OS/0yGGeCtW7cue6JXmnjOlc2oSu+OJPAIDQgjwIAwAgwII8CAMAIMCCPAgDACDAgjwICwMAs5zMw1QDZsKnXMzGM2XzeZTBqOtanFcVfFZWZzR+7eIxuKn8kMxQvwSuPu/YuquiiGCQSP0IAwAgwII8CAMAIMCCPAgDACDAgjwIAwAgwIo5DDFXdVPori2ZmsBPANDAgjwIAwAgwII8CAMAIMCCPAgDACDAgjwIAwAgwIoxLLxNwFqrGxsWgjSeeun5b5jmxWNjSjTmvZCLBJ6XWBctdPq+TXAYwnHqEBYQQYEEaAAWEEGBBGgAFhBBgQRoABYQQYEFa8Qg5zVZONqCqibES1wqCZTR+vqOrDbJTAZ7J4AaZMJx11S+lYb3HZeIQGhBFgQBgBBoQRYEAYAQaEEWBAGAEGhBFgQFiYhRyl19nIXJmUTCYjqQSIqorL5ro2x9rMc+l9JjPQE0tPVHVLNtel8swRHqEBYQQYEEaAAWEEGBBGgAFhBBgQRoABYQQYEFZYIYe7HkJRMfcuMt+vu9ojmx5R7sZsc92oji15Zb7vRz0GVTaFllEVacazOBTLxiM0IIwAA8IIMCCMAAPCCDAgjAADwggwIIwAA8Li0lLHZl08m5IJmzPb1A/ZsLkjd+sA2ozKLKryEnefnBDFJcCKFIuW6E1VYniEBoQRYEAYAQaEEWBAGAEGhBFgQBgBBoQRYEBYYYUc8WzI4q43lZm72XC3pp7NmW3q0syiehfMx0a1CmRBSr8Si9qj4ii9SZb45PAIDQgjwIAwAgwII8CAMAIMCCPAgDACDAgjwICwwhY3c9f3yN1afpbr8VVXV+fae/LkScOZo6rUMd+RDXdVXOaZtHkX3L2DNp+6MGsW/UKEc8mlJJPJZV/X3bE2wpubwsTzjtyNyh2bOyrabPAIDQgjwIAwAgwII8CAMAIMCCPAgDACDAgjwICwwlrqmPseRVUvFVVPrHj2iIpKVOse2lQ1RdUTy3xs3jqQdIUF2Kb+y6bDUFTH2oh/O6VwSXSQKojEHfEIDQgjwIAwAgwII8CAMAIMCCPAgDACDAgjwICwwnpimdl0J7LptqVYp2VmMxs26/GZRTVX7jqxxZPDSiyzqMpWFOu0FDFXMcQjNCCMAAPCCDAgjAADwggwIIwAA8IIMCCMAAPCCivkcFchZMNy/UHDsTZ9rWy6QEV1XXe1Vu46ZtlcN6pObGEqaCk0mwvZrObm7rorTVTvkc274O66UR0bIh6hAWEEGBBGgAFhBBgQRoABYQQYEEaAAWEEGBBWvNUJbdjUtSiu9GfmroNU6c1VVB3RzMeGWJVYvNUJbdCNCS6UwOeKR2hAGAEGhBFgQBgBBoQRYEAYAQaEEWBAGAEGhIW5OmFUK9+Z61rMayYqslnn0V3tkTtR1eG5u25klVjuuKuJUS+1CZdi7VFUY5aYKx6hAWEEGBBGgAFhBBgQRoABYQQYEEaAAWEEGBAW5uqE7ipXoqofsuk+5e5Yd2x6nkVVL2VT/2cjqutmCLMSy13likRNTAmwmWTeo0jwCA0II8CAMAIMCCPAgDACDAgjwIAwAgwII8CAsDALOYrWB6ggNlVc8Vzbzsw8qsbGRsOxMakuyhDbKr1lH2seVUFt6uLSE8udlVYfFs9RuRPV/cZkknmEBoQRYEAYAQaEEWBAGAEGhBFgQBgBBoQRYEBYYasTxrNSx4ZNJyd3qwTacLeSo7tKO3efq3iuMGi+XyqxCsDKd+liOCRL8ZznEPEIDQgjwIAwAgwII8CAMAIMCCPAgDACDAgjwICwuFRi2XQ2impdPHdsVjbM2xPLUfWY+cxmNhVg7ri73xArsTy/EMsb7n+RTCaXfV3zsVHdkY14zobNmW2EN6+FcXe/5usWdCoeoQFhBBgQRoABYQQYEEaAAWEEGBBGgAFhBBgQVryWOjar5plXgnNXqeOuc5VZVLVH5nm2WdnQprYsKhKfq+IF2KY7UVRtjUq+o1KGFXWzEQrxc8UjNCCMAAPCCDAgjAADwggwIIwAA8IIMCCMAAPCCuuJZe5dZGbTQ8jMZlRmNrU47vpamSmuIWgW1UqOUSkokoVVYsWzUieeo7Kx0irAzJgNAx6hAWEEGBBGgAFhBBgQRoABYQQYEEaAAWEEGBBWWCFHPHsX2bCpTHLHXYWQzZltVpB0x6YXl42YzEbxemJBHRVR6WIyGzxCA8IIMCCMAAPCCDAgjAADwggwIIwAA8IIMCAszEKOqNbyM7Psa7XsY23WW3Qnnl2gbNisqBiVmK5OGJPalBC5u50Sm6gIKc4kqxMC8DwCDEgjwIAwAgwII8CAMAIMCCPAgDACDAgrXksdd/203PU9sum35O5YG+7WPXR35nhWj9nMhvlYh6sTQp27arnSq8OzUbTZ4BEaEEaAAWEEGBBGgAFhBBgQRoABYQQYEEaAAWEUciyfTS2OTa8md+vimSvAbO7I5sxm7iqibI51V7WWgQC7Uno1T4p3ZHPmeI4qA4/QgDACDAgjwIAwAgwII8CAMAIMCCPAgDACDAijkMPEvPKdu15NUa24F8+V/myquGzeI3fvLz2xiiSqJk8r7bo2VngvLh6hAWEEGBBGgAFhBBgQRoABYQQYEEaAAWEEGBBWvEIOc5WPjagqhGzYrD8Y1WqMNqLqTWUzKrOorpuheAFeyeUyKPneVFFdl0doQBgBBoQRYEAYAQaEEWBAGAEGhBFgQBgBBoSFWcjhrocQ0kXVi8ssqg5SUVVTmblbmzIDPbHwP4rVcvHsiVW0UfEIDQgjwIAwAgwII8CAMAIMCCPAgDACDAgjwICwsoKWQgMQK3wDA8IIMCCMAAPCCDAgjAADwggwIIwAA8IIMCCMAAPCCDAgjAADwggwIIwAA8IIMCCMAGc6e/bsPffcE/UoPM/zvv3227vvvjvXlviMs2iyJwQEONPCwsLU1NSSu65cufLOO++Yt4RoeHh43759ubYYxrk8N27ceO211zZt2lRZWXnHHXe8/vrrN27cyLurmLInBJ6P/zc9PZ1rWrJ3GX7ZXkNDw/j4eK4toV+6r69v27Ztx48fv3DhwujoaH19/QsvvJB3VzFlTwgIcKaYBHh6enrdunWLi4u5toR+6VtuueXrr79O/Tg6Onrrrbfm3VU02RMC3/d5hF7aL7/88thjj61Zs2bjxo1fffWV53kHDhwI1qQqKysrKyv77LPPsrd4nnfmzJm77rrr+++/f+SRR9asWbNhw4YDBw6kTnv+/PlHH3305ptvrq6ufuihhz7//PNg+19//XXlypX0AQwPD+/du3fVqlWGLUuO0/O806dPP/PMMxs3bqysrHzggQfOnDkTbL/33nvffPPN9MOfe+651H+ky8vLr1+/ntq1fv36f/75J++udLmum3dazHsNtw++gTMF32wNDQ39/f3Dw8O7d+/esmWL7/tzc3NHjhzxPG9ycnJycvLy5cvZW4LDKyoqamtr33777bGxsf7+/oqKikOHDgUn37lz5+OPP37ixIkTJ068+uqrL774YrB9y5Ytt91227Vr11LDaG1tHR4eTh9YxpZc4/R9v6Ojo6en58iRI2NjY3v27GltbQ22DwwMNDQ0pJ+ztrb2rbfeCl4///zzdXV1x44dC37s6el59tln8+5Kl+u6eafFvDfXhMDnETpbEIxkMhn8+NNPP3med/HiRf+/PUIHW2ZnZ1NbXn755V27dgWvE4nEd999l33Rtra2xsbG+fn54MeLFy9WVVWl5zl7i2GcCwsLqV+bmZnxPO/q1au+71+6dCmRSKT+GxksJjozMxP8OD8/Hyy32dLSsnfv3h07dvzxxx95d6XLdd2802Leu+TtI8Aj9NJSa0Nu3rzZ87yrV68WdPjtt9+eet3Z2Tk5ORm8fuKJJ3p6ej788MPff/89/fe/+eab06dP33TTTcGPo6OjHR0d1dXVqV/I3mIYZ2Vlped5Z8+e/eijjwYGBjzPm5ub8zxv7dq13d3dH3/8cXDIyMhIW1vbpk2bUj/Ozc0dPXq0o6NjfHx8ZmYm+EfBvCtdruvmnZa8e3PdPvgGzpT9pep53vT09JK7/suWH374oby8PHi9uLg4ODjY3Ny8evXq3t7eS5cuLTmGrq6u999/37zFMM7Z2dmHH3548+bNfX19H3zwQWq77/tffvllTU1N8FXW0tIyODiYOryurm5kZCR4ffny5c7Ozqampry70hmua54W894lbx8BApwp9ACPjo7W1tZmXCWZTO7cubOjoyN7ANeuXauqqgoehnNtMY+zvb29r68v9Qfb9CAtLi7W19d/+umns7OziUQi9S9I8MSbeob3ff/ChQue583Ozhp2ZYzccF3ztJj3Lnn7CPAIXYDy8nLP89JrGLK3BP7+++/U64MHD7a3t2ds3759+8DAwPj4ePDjr7/+ev78+eD12NhYc3Pzhg0bUmfI3mI2Pj7e29sb/MH2zz//TN+1atWqp59++tChQ0ePHt2zZ8/atWuD7TU1NZ7n/fzzz6nfXFhY8DwvkUgYdmWM3HBd87SY9xZ6+ytL1P+CxI7hm21+fr6qqqq/v//UqVPB35yztwSHt7W1DQ0NHTt2bP/+/atXr56amgpOdf/99w8ODk5NTU1MTHR1dbW3twfb6+rqUk+2+/fvf+ONN9IHkL3FPM6mpqannnoqmUwePnx427ZtiUQi9U3o+/7MzEwikWhtbT18+HD64T09PU1NTWNjY+fOnRsbG7vvvvu6u7vz7kofueG65mkx713y9hEgwJkMwfB9/+DBg+vWrauqqnrvvfeW3BIcPjQ01NzcXF5e3tLScvz48dSpTp061dXVVVNTU1VV1dXVlXoKTf0VenFxcf369anPru/72VvyjnNiYqKxsbGiomLXrl0TExNNTU3pAfZ9f/fu3anUpczPz7/yyiv19fUVFRV1dXUvvfTS9evX8+5K//u54brmaTHszXX7CBDgkFkWSI2Pj995553mLfZ6e3uffPLJcM9pZp4Ww14Xt19KKor+zA6T4eHhrq4u8xZLv/3229DQ0MjISIjndCf02y8x/BErXkZGRjI+r9lblu3HH3/84osvOjs729vbH3zwwVDO6VqIt1+S+AaOl/T64Vxblm3fvn3nzp3r7u5+9913wzqnayHefklifWBAGI/QgDACDAgjwIAwAgwII8CAsH8BaQkuMIIwmRwAAAAASUVORK5CYII=",
];
