import { HttpStatusCode } from "axios";
import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";
import { ref } from "vue";
dayjs.extend(utc);

export class AuthorizationUtil {
  // private static instance: AuthorizationUtil;
  static #instance;
  #token = ref("");
  #admin = ref(undefined);

  static getInstance() {
    this.#instance = this.#instance ?? new AuthorizationUtil();
    return this.#instance;
  }

  getToken() {
    const authJson = localStorage.getItem("auth");
    const cookies = document.cookie.split("; ");
    const cookieObj = cookies.reduce((acc, cookie) => {
      const [name, value] = cookie.split("=");
      acc[name] = value;
      return acc;
    }, {});
    this.#token.value = cookieObj["token"];
    const auth = JSON.parse(authJson);
    const token = auth ? auth.accessToken : "";
    return token;
  }

  setToken(token) {
    this.#token.value = token;
    document.cookie = `token=${token}; expires=${dayjs()
      .add(process.env.VUE_APP_UQEY_EXPIRE_TOKEN_TIME_MINUTES, "minute")
      .utc()}; path=/`;
  }

  setAdmin(value) {
    this.#admin.value = value;
  }

  isAdmin() {
    return this.#admin.value;
  }

  checkResponse(response) {
    const statusCode = this.extractStatusCode(response);

    if (HttpStatusCode.Unauthorized === statusCode) {
      this.setToken("");
      return undefined;
    } else if (HttpStatusCode.Forbidden === statusCode) {
      return location.assign(`${location.origin}/403`);
    } else {
      this.setToken(this.getToken());
      return response;
    }
  }

  extractStatusCode(response) {
    try {
      if (response instanceof ArrayBuffer) {
        const text = String.fromCharCode.apply(
          null,
          Array.from(new Uint8Array(response))
        );
        const data = JSON.parse(text);
        return data.status;
      } else {
        return response.status;
      }
    } catch (e) {
      return response.status;
    }
  }
}
