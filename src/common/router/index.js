import { createRouter, createWebHashHistory } from "vue-router";
import Home from "../../pages/Home/pages/index.vue";
import Charge from "../../pages/User/pages/Charge/index.vue";
import Purse from "../../pages/User/pages/Purse/index.vue";
import Withdrawal from "../../pages/User/pages/Withdrawal/index.vue";
import Page404 from "../layouts/404.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/user",
    children: [
      {
        path: "",
        name: "NotFound",
        component: Page404,
      },
      {
        path: "charge",
        name: "Charge",
        component: Charge,
      },
      {
        path: "purse",
        name: "Purse",

        component: Purse,
      },
      {
        path: "withdrawal",
        name: "Withdrawal",

        component: Withdrawal,
      },
    ],
  },
];
const router = createRouter({ history: createWebHashHistory(), routes });

export default router;
