export const MENU = [
  { label: "Thông tin cá nhân", icon: "mdi-account-circle", key: "account" },
  { label: "Quỹ tiền", icon: "mdi-cog-outline", key: "money" },
  { label: "Lịch sử", icon: "mdi-bitcoin", key: "history" },
  { label: "Hòm thư", icon: "mdi-bell-badge", key: "mail" },
  { label: "Trung tâm trợ giúp", icon: "mdi-book-variant", key: "help" },
  { label: "Xóa bộ nhớ cache", icon: "mdi-refresh", key: "clearCache" },
  { label: "Đăng xuất", icon: "mdi-logout", key: "logout" },
];
