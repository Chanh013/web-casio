import { acceptHMRUpdate, defineStore } from "pinia";
import { ref } from "vue";

export const useSnackbars = defineStore(
  "snackbars",
  () => {
    const snackbars = ref({ isOpen: false, text: "" });

    const openSnackbar = (status, text) => {
      snackbars.value = {
        isOpen: true,
        text,
        status,
      };
    };

    const closeSnackbar = () => {
      snackbars.value = {
        isOpen: false,
        text: "",
      };
    };

    return {
      snackbars,
      openSnackbar,
      closeSnackbar,
    };
  },
  {
    persist: true,
  }
);
if (import.meta.hot)
  import.meta.hot.accept(acceptHMRUpdate(useSnackbars, import.meta.hot));
