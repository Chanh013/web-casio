import BaseService, { METHODS } from "./BaseService";

class AuthorizationService extends BaseService {
  constructor(prefix) {
    super(prefix);
  }

  async checkPermission() {
    return await this.performRequest(METHODS.POST, "auth");
  }
}
export default new AuthorizationService("");
