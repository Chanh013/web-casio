import { AuthorizationUtil } from "../utils/Authorization";
import http from "./AxiosService.js";

export const METHODS = {
  GET: "GET",
  POST: "POST",
  DELETE: "DELETE",
  PUT: "PUT",
};

export default class BaseService {
  #prefix = "";
  #authorizationUtil = AuthorizationUtil.getInstance();
  constructor(prefix) {
    this.#prefix = prefix;
  }

  async performRequest(method, url, data, headers, responseType) {
    // if (!this.#authorizationUtil.getToken()) {
    //   return undefined;
    // }
    const endPoint = this.#prefix + (url ? `/${url}` : "");
    const options = {
      method,
      url: endPoint,
      data,
      headers,
      responseType,
    };
    options[method === METHODS.GET ? "params" : "data"] = data;
    const response = await http(options);

    return this.#authorizationUtil.checkResponse(response.data);
  }
}
