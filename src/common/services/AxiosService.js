import axios from "axios";

import { AuthorizationUtil } from "../utils/Authorization";

const http = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
});

http.interceptors.request.use(async (config) => {
  const header = {
    Accept: "application/json",
    Authorization: `Bearer ${AuthorizationUtil.getInstance().getToken() ?? ""}`,
  };
  config.headers = header;
  return config;
});

export default http;
