import { acceptHMRUpdate, defineStore } from "pinia";
import { ref } from "vue";
import BankingService from "../services/BankingService";

export const useBank = defineStore(
  "bank",
  () => {
    const banks = ref("");

    async function getBanks() {
      const result = await BankingService.getBanking();
      if (result) {
        banks.value = result.items;
      }
      return result;
    }

    return {
      getBanks,
      banks,
    };
  },
  {
    persist: true,
  }
);
if (import.meta.hot)
  import.meta.hot.accept(acceptHMRUpdate(useBank, import.meta.hot));
