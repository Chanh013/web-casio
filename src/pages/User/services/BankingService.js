import BaseService, { METHODS } from "../../../common/services/BaseService";

class BankingService extends BaseService {
  constructor(prefix) {
    super(prefix);
  }

  async getBanking() {
    return await this.performRequest(METHODS.GET, "v1/banks");
  }
}
export default new BankingService("");
