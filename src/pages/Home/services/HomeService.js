import BaseService, { METHODS } from "../../../common/services/BaseService";

class HomeService extends BaseService {
  constructor(prefix) {
    super(prefix);
  }

  async login(data) {
    return await this.performRequest(METHODS.POST, "v1/auth/login", data);
  }
  async register(data) {
    return await this.performRequest(METHODS.POST, "v1/auth/register", data);
  }
}
export default new HomeService("");
