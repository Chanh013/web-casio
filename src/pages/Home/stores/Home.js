import { acceptHMRUpdate, defineStore } from "pinia";
import { ref } from "vue";
import HomeService from "../services/HomeService";

export const useHome = defineStore(
  "auth",
  () => {
    const accessToken = ref("");
    const user = ref("");

    async function login(data) {
      const result = await HomeService.login(data);
      if (result) {
        accessToken.value = result.token.accessToken;
        user.value = result.user;
      }
      return result;
    }
    async function register(data) {
      const result = await HomeService.register(data);
      if (result) {
        accessToken.value = result.token.accessToken;
        user.value = result.user;
      }
      return result;
    }
    return {
      accessToken,
      user,
      login,
      register,
    };
  },
  {
    persist: true,
  }
);
if (import.meta.hot)
  import.meta.hot.accept(acceptHMRUpdate(useHome, import.meta.hot));
