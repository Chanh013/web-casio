import { createApp } from "vue";
import App from "./App.vue";
import vuetify from "./plugins/Vuetify";
import pinia from "./plugins/Pinia";
import router from "./common/router";
import './plugins/veeValidate'

const app = createApp(App);
app.use(router).use(vuetify).use(pinia).mount("#app");
