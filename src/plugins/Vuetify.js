import { createVuetify } from "vuetify";
import { en } from "vuetify/locale";
import "@mdi/font/css/materialdesignicons.css";
import * as components from "vuetify/components";
import * as labsComponents from "vuetify/labs/components";
import * as directives from "vuetify/directives";

import "vuetify/styles";

const vuetify = createVuetify({
  icons: {
    defaultSet: "mdi",
  },
  components: {
    ...components,
    ...labsComponents,
  },
  directives,
  locale: {
    locale: "en",
    fallback: "en",
    messages: {
      en,
    },
  },
  theme: {
    defaultTheme: "dark",
  },
  display: {
    mobileBreakpoint: "sm",
  },
});

export const vuetifyConfig = (state) => ({
  props: {
    "error-messages": state.errors,
    modelValue: state.value,
  },
});

export default vuetify;
